# AESIR #



### What is AESIR? ###

**AESIR** (**A**utomated **E**nvironmental **S**urveillance & **I**nteractive **R**eports) is a project 
that monitors the environment and generates telemetry & datamaps using **Sentienz Akiro IoT** platform and **Esri GIS**.

**AESIR** accesses telemetry data from IoT devices attached to things (buildings, parks, vehicles, etc.) 
that measure and monitor natural processes (temperature, wind velocity, humidity, etc.) in the surrounding environment.

**Sentienz Akiro IoT** platform provides interactive dashboards.

**AESIR** creates AI / ML models in the form of **Esri GIS** datamaps that analyze, detect, and predict critical ecosystem changes.

Integration with green technologies (solar, wind, geothermal) will provide solutions to the adverse effects of climate change.